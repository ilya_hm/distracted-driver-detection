# Distracted Driver Detection
This repository use deep learning architecture (CNN) to classify state farm distracted driver dataset and compare the results with Random Forest algorithm.

### Table Of Contents:
- [Description](#description)<br>
    - [About the project](#about-the-project)<br>
- [Data](#data)<br>
    - [Dataset Example](#example-of-dataset)<br>
    - [Files](#files)<br>

 - [Requirements](#requirements)<br>

----

### Description

#### About the project
This study is about distracted driver detection, we have an RGB image dataset and the idea is to predict the class of driver in order to improve road safety.

----

### Data
25000 Images of vehicles driver, you can find the dataset here: https://www.kaggle.com/c/state-farm-distracted-driver-detection.

this dataset contain 10 classes which are:

* c0: safe driving
* c1: texting - right
* c2: talking on the phone - right
* c3: texting - left
* c4: talking on the phone - left
* c5: operating the radio
* c6: drinking
* c7: reaching behind
* c8: hair and makeup
* c9: talking to passenger


#### Example of dataset

Example of dataset 1             |  Example of dataset 2
:-------------------------:|:-------------------------:
![Drag Racing](images/imagesample1.jpg)  |  ![Drag Racing](images/imagesample2.jpg)

#### Files

- `CNN-Architecture-1.ipynb`: This notebook contains a first attempt of a CNN to claasify images.
- `CNN-Architecture-2.ipynb`: This notebook contains an improvement of the first implementation by adding more layers.
- `Random Forest.ipynb`: This notebook contains a Random forest implementation.

----

### Requirements

- [Python >= 3.5](https://www.python.org/downloads/release/python-366/)      
- [Tensorflow](https://www.tensorflow.org/)                                            
- [tqdm](https://tqdm.github.io/)                                        
- [opencv](https://opencv.org/)   
- [scikit-learn](http://scikit-learn.org/stable/)                           

----
